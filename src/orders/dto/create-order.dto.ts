import { Type } from 'class-transformer';
import {
  IsArray,
  IsNotEmpty,
  IsPositive,
  Min,
  ValidateNested,
} from 'class-validator';

class CreatedOrderItemDto {
  @IsPositive()
  @IsNotEmpty()
  productId: number;

  @IsPositive()
  @Min(0)
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsPositive()
  @IsNotEmpty()
  customerId: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreatedOrderItemDto)
  orderItems: CreatedOrderItemDto[];
}
